CREATE DATABASE chatRoom;

CREATE TABLE messages (
    sysTime time,
    sender text,
    content text
);
