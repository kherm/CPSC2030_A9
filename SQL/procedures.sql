DELIMITER //

CREATE OR REPLACE PROCEDURE getMessagesAfter (IN enteredTime text)
BEGIN
    SELECT * FROM messages WHERE sysTime >= enteredTime
    ORDER BY sysTime;
END //


CREATE OR REPLACE PROCEDURE getNewestMessages ()
BEGIN
    SELECT * FROM (
        SELECT * FROM messages WHERE sysTime > DATE_SUB(NOW(), INTERVAL 1 HOUR)
        ORDER BY sysTime DESC LIMIT 10) sub
    ORDER BY sysTime ASC;
END //


CREATE OR REPLACE PROCEDURE addMessage (IN sender text, IN content text)
BEGIN
    INSERT INTO messages VALUES (NOW(), sender, content);
END //

DELIMITER ;