$(document).ready(function() {
    addMessageListener();
    autoUpdate();
});


function addMessageListener() {
    const msg = document.querySelector('input[name=newText]');
    const user = document.querySelector('input[name=userName]');

    $(msg).click(function() {
        $(msg).prop("value", "");
    });

    $(msg).keypress(function(e) {
        let key = e.keyCode;
        if (key === 13) {
            if ($(user).val() === "") {
                $('.error').html("Please enter a user name.");
                $(user).css("border", "5px solid red");
            } else {
                $(user).css("border", "1px solid black");
                sendMsg($(user).val(), $(msg).val());
                $(msg).prop("value", "");
                $('.error').html("");
            }
        }
    });
}

function sendMsg(user, content) {
    let firstMsgTime = getFirstMsgTime();
    $.ajax({
        method: "POST",
        url: "index.php",
        data: {
            newMessage: content,
            user: user,
            time: firstMsgTime
        },
        success: function(json) {
            updateHTML(json);
        }
    });
}

function autoUpdate() {
    let firstMsgTime = getFirstMsgTime();
    setInterval(function() {
        $.ajax({
            method: "POST",
            url: "index.php",
            data: {
                action: "refresh",
                time: firstMsgTime
            },
            success: function(json) {
                updateHTML(json);
            }
        });
    }, 500);
}

function getFirstMsgTime() {
    const times = Array.from(document.querySelectorAll('.time'));
    if (times.length > 0) {
        return `${times[0].innerHTML}`;
    } else {
        return '00:00:00';
    }
}

function updateHTML(json) {
    $('.pastMessages').html(json);
    let messageBody = document.querySelector('.pastMessages');
    messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight;
    //messageBody.scrollTop(messageBody[0].scrollHeight);
}
