<?php

function clearConnection($mysql) {
    while($mysql->more_results()) {
        $mysql->next_result();
        $mysql->use_result();
    }
}

function connectToDatabase() {
    $user = 'CPSC2030';
    $pwd = 'CPSC2030';
    $server = 'localhost';
    $dbName = 'chatRoom';

    $conn = new mysqli($server, $user, $pwd, $dbName);
    if ($conn->connect_error) {
        die("Connection failed" . $conn->connect_error);
    } else {
        return $conn;
    }
}

?>