<?php

require_once 'sqlhelper.php';
require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);

$conn = connectToDatabase();

if (isset($_POST["newMessage"]) && isset($_POST["user"])) {
    $newMessage = $_POST["newMessage"];
    $user = $_POST["user"];
    $enterMessage = $conn->query("call addMessage(\"$user\", \"$newMessage\")");\
    clearConnection($conn);
}

if (isset($_POST["action"]) || isset($_POST["newMessage"])) {
    $time = $_POST["time"];
    $chat = $conn->query("call getMessagesAfter(\"$time\")");
    clearConnection($conn);
} else {
    $chat = $conn->query('call getNewestMessages()');
    clearConnection($conn);
}

if($chat) {
    $messages = $chat->fetch_all(MYSQLI_ASSOC);

    if (!isset($_POST["newMessage"]) && !isset($_POST["action"])) {
        $homePage = $twig->load('client.html.twig');
        echo $homePage->render(array("messages"=>$messages));
    } else {
        $displayChat = $twig->load('chat.html.twig');
        echo $displayChat->render(array("messages"=>$messages));
    }


} else {
    echo "error";
}

?>